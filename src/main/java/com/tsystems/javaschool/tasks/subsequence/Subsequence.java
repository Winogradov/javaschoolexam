package main.java.com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.LinkedList;
import java.util.Queue;

public class Subsequence {

    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {


        if (x == null || y == null) throw new IllegalArgumentException();


        if (x.isEmpty()) return true;

        if (x.size() > y.size()) return false;


        Queue yQueue = new LinkedList<>(y);
        Queue xQueue = new LinkedList<>(x);

        Object xItem;
        Object yItem;

        xItem = xQueue.remove();

        while (!yQueue.isEmpty()) {
            yItem = yQueue.remove();

            if (yItem.equals(xItem)) {
                if (xQueue.isEmpty()) return true;

                xItem = xQueue.remove();
            }
        }

        return false;
    }
}